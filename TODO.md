# carbonOS TODO List

This file describes carbonOS's roadmap for the next few releases. It is being
continuously updated. Once carbonOS switches to different infrastructure, this
file will be replaced with a proper task management system (eg Gitlab issues/boards)

---

- systemd missing libraries
- tpm2 in more places (fwupd? systemd?)
- ppp in more places (networkmanager, modemmanager)
- Drop bdb: https://gitlab.gnome.org/GNOME/gnome-build-meta/-/commit/e6764356e5549b8244963fd6855900f6739a809a

# Implemented things to test

# Things that need to be cleaned up

---

# carbonOS 2021.2 (Beta)

- Add bug report URL to os-release
- Unify SBAT settings, to allow for easier vendor customization
- Re-enable natuilus plugin in gnome-terminal

##### Graphical Session

- Fork gnome-settings
- Fixup broken spacing in gde-panel's control center with only 2 items
- Update gde-gnome-bridge's output display name logic
- gde-panel: Don't try to launch bluetooth on unsupported hardware
- Implement network manager support in gde-dialogs & drop nm-applet
- Folders in gde-panel launcher
- Prevent accidental notification touches
- Hook udisks2 up to gde (unmount applet, unlock volume prompts, mount/unmount sounds)
- Save "stay awake" state across reboots (proposal)
- Stay Awake prevents shutdown
- Finish gde-gnome-bridge
- "What's new"
    - Yelp page of the changelog (translated, hopefully)
    - On session start, compare gsetting to /etc/os-release's VERSION
    - If /etc is newer, launch yelp. Else do nothing
    - Update the gsetting
- Package gnome-music and gnome-videos (or install as flatpaks?)
- Customize the preview-indication-view in Wayfire
    - Adwaita colors, or just make them customizable
    - Fade out close animation. Zoom+Fade in open animation
    - When the same window causes a new indication, simply transition its size
      instead of animating from scratch
- Custom window open/close animations in Wayfire

##### System

- Move packages out of early build environment/bootstrap if appropriate:
    - openssl
    - popt
    - bzip2
    - lua
    - file

- Maybe pick a better shell (possibly default):
    - Pick fish shell (https://fishshell.com) or zsh (https://zsh.sourceforge.io/)
    - If zsh: Decide if this is a devel or base image thing
    - If fish: We need to port all the scripts in /etc/profile.d to be fish-compatible
    - We _could_ go with both: fish on desktop and zsh on devel image. However, sounds like a lot of configuration options to worry about

- Come up with a better way to get cached bootstrap
    - Maybe just pull down fdo's bootstrap base and call it a day? Are there enough packages in there?
    - Split the bootstrap into its own project, update it less regularly, etc?

- Enable gnome-terminal search provider

- Mark boot successful if user session has run for a few minutes
- Download flatpaks from flathub, then bake them into the installer image OOTB
- Split libsoup into `libsoup2` and `libsoup` (which will be v3+)

---

# At some point

### Graphical

- Boot animation for vendors: we'd want some way to configure vendor installs to have a "Powered by carbonOS"
 watermark?
- Crash handler GUI (???) Integrate into systemd-coredump?
- Package wayvnc?? (gnome-remote-desktop just uses pipewire so we can probably just do that?)
- opendyslexic rewrite

### Misc.

- Once ported to bst2, remove the /dev/shm handling in buildsystems/build-links.bst
- Replace '' with "" throughout the codebase
- Replace our horrible build-dir mangling for autotools w/ command-subdir + conf-cmd combo
- Does gamemode actually work?
- Does fwupd actually work?
- Google safe-browsing API key
- Make sure ssh root password login is disabled
- Move nano's EDITOR=nano into its own profile.sh script
- Compress firmware
- Upgrade to openssl 3.0
- Package SANE (scanning) and simple-scan (?)
- Package intel's thermald
- Package libfprint
- Package pykeepass
- Implement systemd-homed once somewhat supported upstream
    - Drop shadow & related cruft
- Generate yelp documentation file for all the copyrights
- Remove IWD workaround patch once it is fixed upstream
- Package gnome-photos when it replaces eog
- User/version tally system
- Package/Set up apparmor for enhanced sandboxing and security
- Upgrade to fuse3 where possible
    - Not possible: ostree, ntfs-3g (check these)
- Put git in the bootstrap and remove some git-related cruft from the early build environment
- Enable LTO system-wide (right now we're just doing meson)
- Wayland-only: https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/454/diffs
- https://systemd.io/COREDUMP_PACKAGE_METADATA/
- Enable -Dauto_features=enabled in meson
- Automated tests w/ openqa: https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/1251

### Look into

- Add libphonenumber as a dep of evolution-data-server; depends on protobuf
- Is there some way to avoid sed-ing GCC's sources to point to the dynamic loader
- TPM support?
    - fwupd
    - where else?
- Fido security tokens (openssh, greeter)?
- Should we build/include man pages & man itself? It seems that a bunch of stuff assumes man is functional
	- If we do include man, dont include man books about C code and stuff like that. Just config files & binaries in /usr/bin
- Postscript support (evince)?
- libopenraw?
- Packages that are known to still require GDK-X11:
    - gnome-settings-daemon
    - gcr
    - libcanberra
- Bonsai
- cloudproviders
- cloudprint?
- CUPS from OpenPrinting and not from Apple upstream??
- Package neard (nfc)?
- Package nice/extra thumbnailers
    - https://gitlab.gnome.org/ZanderBrown/gnome-source-thumbnailer
    - https://github.com/exe-thumbnailer/exe-thumbnailer
- Bottles app for windows compat (build it into the OS somehow?)
- Risc-v support https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/merge_requests/4532/diffs#fff54af28a9e2050dcbdf50aca48d96fdda70857
- https://fleet-commander.org/
- Extensions as layers in os-updated OR as overlays via systemd-sysext

### /usr/libexec/esp-update (NEEDS SOME REDESIGN)

- Will be used to keep bootloader, shim, and ext4 FS driver up-to-date. Potentially can be used with fwupd also (I'll have to do some research into how fwupd updates itself today)
- systemd should be configured to mount the ESP read-only on /efi. Nothing but this tool should be touching the ESP post-install.

- Reads configuration file: /etc/esp-update.conf. Format: `[FROM] [TO] [VARIABLE]`. `FROM` is an absolute path relative to the OS root,
  and `TO` is an absolute path relative to the ESP. `VARIABLE` is the name of an EFI variable into which to put `TO`, and is optional.
  If you do not have a variable value to set, `VARIABLE` should equal `-`. Setting `VARIABLE` updates the behavior of the tool to
  be more atomic.
- For example: `/usr/lib/systemd/boot/efi/systemd-bootx64.efi /EFI/BOOT/BOOTX64.EFI` will keep systemd-boot up to date.
- Enters a mount namespace and remounts /efi rw
- Compares the checksum of the existing file (if it exists) and the potentially new file. If they don't match, continue with that file
- Copy the file into the ESP into the correct location (PATH from here on out) with a .staged file extension
- `sync` the file into place
- Try to glnx_renameat2_exchange(PATH.staged -> PATH), and if ENOENT do glnx_renameat2_noreplace(PATH.staged -> PATH)
- `sync` the renames into place
- If it still exists, delete PATH.staged
- `sync`

##### Compatability layers (MAY NEED SOME REDESIGN)

- Based around flatpak runtimes
- Shared GUI component:
    - "This program was not designed to run on your system, but carbonOS can try
    to run it with a compatiblity layer. Install the necessary files?"
    - This program was not designed to run on your system, but carbonOS can try to run it in
    compatability mode. The following components are missing from your system: Microsoft Windows™ Runtime (506 mb). Install them?
    - Dropdown of "more details" showing exactly what it wants to install, from
    where, and what it's trying to run
    - If user hits install, progress bar of installing the runtime. Then close
    and run the program immediately
- Files live in `/usr/libexec/compat/wrap-FMT`
    - FMT: jar, appimage, exe, x86 (32-bit program), apk, rpm, deb, aarch64, [...]
- Wrapper executes the GUI to install the runtime if necessary, and then
executes the program from within the runtime
- Use binfmt.d to wrap the incompatible program with the file from libexec
