# Keys

This folder contains the cryptographic material required sign carbonOS and its various sub-components.

## Expected keys

There are various things that may need to be signed for carbonOS. This section describes what these
keys are and where they should be located.

- `repo/`: GPG keys for signing the repository's `SHA256SUMS` file
	- `pubkey.gpg`: The GPG public key, in a format that can be consumed by `gpg --import`
	- `privkey.gpg`: (OPTIONAL) The GPG private key, in a format that can be consumed by `gpg --import`. If not found, GPG will try
	  to use a connected Smart Card (i.e. a Yubikey or similar)
- `sign/`: X.509 signing keys, RSA-2048, shared among all units
	- `root.cert`: PEM-encoded Root CA's X.509 certificate. This is the certificate the system will be configured to trust
	- `pubkey.cert`: PEM-encoded X.509 certificate of the signing key
	- `privkey.pem` or `privkey.pkcs11`: PEM-encoded private key or pkcs11 URI to a key in an HSM (i.e. a Yubikey or similar)
- `$BOARD/$VARIANT/`: RSA-2048 keypair for signing TPM PCRs
	- `pubkey.pem`: PEM-encoded public key
	- `privkey.pem`: PEM-encoded private key
	- `decrypt.gpg`: (OPTIONAL) A GPG public key, in a format that can be consumed by `gpg --import`. If present, `privkey.pem` is
	  decrypted with the private key that corresponds to this public key (which must be located on an HSM, such as a Yubikey or similar)
- `uefi/`: UEFI singing keys for testing in VM. NOT TO BE USED IN PRODUCTION
	- `PK.{pem,cert,auth}`: Platform Key (PEM-encoded RSA-2048 private key, PEM-encoded X.509 certificate, and signed EFI var)
	- `KEK.{pem,cert,auth}`: Key Exchange Key (ditto)
	- `DB.{pem,cert,auth}`: Allowlist Database (ditto)

## Keeping Keys Safe

If you're just doing local development builds of carbonOS, then you can just store the keys unencrypted in this directory. However,
if you're doing anything more than that, you should handle the keys securely, and with care. Here are some tips on keeping your
keys safe, assuming you're building carbonOS for distribution. This is based on our own key management techniques.

- Use HSMs (like YubiKey, or similar) as much as possible!
- Generate keys **offline**
- Use multiple layers of keys! Your root private key should exist only in an HSM, and stored in a **physically secure**
  location (like a Bank Safety Deposit box).
  - Note: GPG only allows two layers
- Rotate your subkeys regularly
