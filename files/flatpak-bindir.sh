if command -v flatpak > /dev/null; then
    # set PATH to include Flatpak installations
    new_dirs=$(
        (
            unset G_MESSAGES_DEBUG
            echo "${XDG_DATA_HOME:-"$HOME/.local/share"}/flatpak"
            GIO_USE_VFS=local flatpak --installations
        ) | (
            new_dirs=
            while read -r install_path
            do
                bin_path=$install_path/exports/bin
                case ":$PATH:" in
                    (*":$bin_path:"*) :;;
                    (*":$bin_path/:"*) :;;
                    (*) new_dirs=${new_dirs:+${new_dirs}:}$bin_path;;
                esac
            done
            echo "$new_dirs"
        )
    )

    PATH="${new_dirs:+${new_dirs}:}${PATH}"
    export PATH
fi
