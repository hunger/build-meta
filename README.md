# carbonOS

carbonOS is a Linux distribution designed from the ground-up to be robust,
modern, easy-to-use, and compatible with a wide range of software. Read more
[here](https://carbon.sh)

## Project structure

- `boards/`: Target devices supported by the OS. See README in this folder for more detail
- `elements/`: [BuildStream](https://buildstream.build) elements that define the OS's components
    - `pkgs/`: Defines all of the individual packages that make up the OS
    - `buildsystems/`: Convenient way to import all the dependencies for a standard build system (meson, autotools, cmake, etc)
    - `groups/`: The basic package groups that carbonOS is split into. Everything that is pulled into an image is in a group
    - `variants/`: Variants of the base OS
    - `junctions/`: Contains the definitions of all the junctions used in carbonOS (bootstrap, plugin sources, etc)
    - `tools/`: Elements that are there to help the build in some way and shouldn't be treated normally
- `files/`: Various auxiliary files that are part of carbonOS's build (i.e. default config)
- `include/`: YAML fragments that are reused in `elements/` and `project.conf`
- `keys/`: Secure-boot, repo, etc. signing keys
- `patches/`: Patches that get applied onto packages in `elements/pkgs/`. Ideally kept to a minimum
- `plugins/`: Custom BuildStream plugins that are used in `elements/`
- `project.conf`: The BuildStream project configuration
- `*.refs`: Used by BuildStream to keep track of the versions of various components
- `result/`: Standard location that `just checkout` exports BuildStream artifacts to
- `pub/`: Local repository, to be served over HTTP or pushed to a remote
- `justfile`: Defines recipies used to build carbonOS
- `tools/`: Scripts used by the `justfile`

## Build instructions

#### Background

In many ways, carbonOS is a normal Linux distro. Much of the prerequiste
knowledge required for any distro development applies here too. However,
on top of that, carbonOS is built around many of the specifications designed
by the [uapi-group](https://uapi-group.org/specifications/specs/). It is
crucial that you familiarize yourself with these specifications, at least
at a high level. The concepts outlined in these specifications are used
throughout the code base:

- The build system produces binaries adhereing to these specs
- The updater consumes binaries adhering to these specs
- The on-disk layout of an installed system adheres to these specs
- The specs are referred to, by their abbreviated name, throughout
  the code-base.

Specifically, carbonOS makes use of and regularly references:

- Discoverable Partitions Specification (DPS): A standard describing how to
  lay out partitions on disk such that they are self-describing and can thus
  be automatically handled.
- Discoverable Disk Images (DDI): A GPT disk image, whose partitions adhere to
  the DPS, and thus can be used correctly in a wide variety of contexts.
- Boot Loader Specification (BLS): A standard describing how the bootloader
  should function, including how files should be laid out such that they can
  successfully be booted.
- Unified Kernel Images (UKI): A standard describing how to combine the Linux
  kernel, initramfs, and other resources into a single EFI/PE binary.

#### Dependencies

First, you need to install build dependencies. For containerized workflow, this
is simple. Here's the recommended method:

```
$ toolbox create --image registry.gitlab.com/carbonos/build-meta carbonOS-devel
OR
$ distrobox create --image registry.gitlab.com/carbonos/build-meta --name carbonOS-devel
```

Then you can enter the carbonOS build environment:

```
$ toolbox enter carbonOS-devel
OR
$ distrobox enter carbonOS-devel
```

This should be enough for carbonOS development. If you are not interested in using
a containerized workflow to build carbonOS, you'll need to install the following
packages:

- [just](https://just.systems)
- buildstream 2.0+
- cargo (only if tracking)
- go 1.14+
- python3-dulwich
- python3-packaging
- python3-requests

If you're going to be using `just run-vm`, you also need:

- qemu (w/ support with the architecture you're compiling for)
- qemu-img
- swtpm
- edk2-ovmf
- python3-virt-firmware

#### Configuring the repo

First you're going to want tell carbonOS's build system about your signing keys.
The build system does **not** support producing unsigned builds of the system.
Please see [`keys/README.md`](keys/README.md) for an explaination on how to
populate the `keys/` directory. However, if you're just building carbonOS for
testing, you can use the following script:

```
$ just keygen
DO NOT USE THIS IN PRODUCTION! Type YES if you understand: YES
[...]
```

Next, you're probably going to want to change some build settings. These
settings can be found in `include/settings.yml`. If you change anything,
make sure to run `git update-index --skip-worktree include/settings.yml`
to make git ignore your changes. If the file changes upstream, you will
need to manually reconcile the changes. NOTE: This situation will improve
in the future, once BuildStream gains the ability to have `type: string`
options.

Finally, you should configure BuildStream using its configuration file. You
can read more about it [at BuildStream's docs](https://docs.buildstream.build).

#### Deciding on your target

carbonOS is split into **variants** and **baords**. A pairing of a
board with a variant is called a **unit**. Units uniquely map to an
OS image that can be built, distributed, and updated.

Boards are target devices that can run carbonOS. They could be as
generic as a whole class of devices (i.e. x86_64 PCs), or they could
be as specific as a single model (i.e. Raspberry Pi 4), or anywhere
in between.

Variants are similar to what some other distros may call "flavors"
or "spins": they're alternative editions of carbonOS that use
different desktop environments or have some other major difference
along those lines.

To determine which board you need to build, you should browse through
the `elements/boards/` directory. Each board will have a README file
describing what kind of device it is suitible for, how to deploy it,
etc.

To select a variant, browse through `elements/variants/` and pick the
one you need. Do keep in mind, however, that not all boards implement
all variants!

From here on out, I'll refer to the board you've selected as `BOARD`
and the variant you've selected as `VARIANT`.

#### Building

Next, you can build the OS. This will take a couple of hours, or more
depending on your hardware. This will also require at least 100GB of
free space on disk. Run:

```bash
$ just build BOARD/VARIANT
[...]
```

#### Publishing

Once your build finishes, you'll be able to publish it to your local
repository, located in pub/. Run:

```bash
$ just publish BOARD/VARIANT
[...]
```

This command will:
- Sign everything that needs to be signed with your keys
- Generate a DDI
- Publish the DDI, along with the files necessary to update existing carbonOS
  installations, into pub/current/

You can find the generated DDI in:
`pub/img/VERSION/carbonOS_VERSION_BOARD_VARIANT.img.xz` relative to the
project root, where `VERSION` is the current version of carbonOS you're building.
Do note that this file is _compressed_ with XZ to collapse down sparse sections,
and some tools may not expect that.

You can also serve the pub/ directory over HTTP, which should permit system updates
to work correctly (assuming `updates-url` in `include/settings.yml` is correctly
configured to point to the HTTP host).

#### Delta Updates

TODO! These aren't implemented yet

Idea: Run a command like `just delta BOARD VARIANT TO_VERSION FROM_VERSION...`

#### Putting it all together

You can combine many of these steps (building, publishing) with `just make BOARD/VARIANT`.

Run `just help` for more information!

#### Debuginfo

Debuginfo gets special treatment in carbonOS, because it is an extemely large but seldom-used
resource that is, at times, still necessary. Thus, we'd like to avoid distributing debuginfo
in any image. carbonOS uses [debuginfod](https://sourceware.org/elfutils/Debuginfod.html) to
serve debuginfo to debuggers on-demand, which allows for both a debuginfo-free OS and rich interactive
debugging. To build carbonOS's debuginfo bundle, run:

```bash
$ TODO
[...]
$ just checkout tools/debuginfo.bst
[...]
$ ls result/
TODO
```

I'll leave configuring `debuginfod` to consume this bundle as an excercise to the reader. Once you have
`debuginfod` set up on a server, just put the domain (i.e. `https://debuginfod.carbon.sh/`) into
`/etc/debuginfod/your-company.urls` and carbonOS will start querying your server for debuginfo.

#### Building everything

If you are redistributing carbonOS and want to build and publish all available units (per-arch),
you can do so with the following command:

```bash
$ just make @ARCH
```

Run `just help` for a description of what patterns are accepted by commands like `just build`, `just publish`,
and `just make`.

#### Virtual Machine

You can test your build of carbonOS with a local virtual machine. Use:

```
$ just make BOARD/VARIANT
[...]
$ just create-vm BOARD/VARIANT
```

to create the virtual machine. Once it is created it'll automatically run. Once the VM is stopped,
you can start it up again by running: `just run-vm`.
