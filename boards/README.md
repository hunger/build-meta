# Boards

This directory defines the various boards that carbonOS can target.

## Definition file

The board definition file describes the properties of the board in a
machine-readable way. Specifically, this file is a shell script, but
it should consist entirely of variable declarations.

Properties that can be defined:

- `ARCH`: **(REQUIRED)** The architecture supported by this board
	- Valid values: amd64
	- In the future more architectures will be supported

- `VARIANTS`: **(REQUIRED)** The variants supported by this board
	- Bash array of variant names (see elements/variants/*.bst for list of variants)

- `PKGS`: Packages that should be included for this board
	- Bash array of BST element names (i.e. boards/myboard/pkgs/foobar.bst)

- `PKGS_$variant`: Like `PKGS`, but only includes packages for given variant

- `CMDLINE`: Arguments to be appended to the kernel's command line

- `CMDLINE_$variant`: Like `CMDLINE`, but only applies to the
  given variant

Here's an example instance of this file:

```bash
ARCH=amd64
VARIANTS=(gnome kde)

CMDLINE="substem.feature=1"
CMDLINE_gnome="gdm.do_something"

PKGS=(
	boards/myboard/pkgs/somepkg.bst
)
PKGS_gnome=(
	pkgs/someotherpkg.bst
	boards/myboard/pkgs/gnomepkg.bst
)
```

## Directory Layout

- `BOARD`: **(REQUIRED)** The board definition file, as described above
- `pkgs/`: Optional elements that define board-specific packages.
  - Put packages in here when they only make sense for a given board
  - If a package is generally useful, put it in the elements/pkgs directory
  - Example: Device-specific bootloaders like m1n1 or tools like raspi-config
- `files/`: Optional files supporting the elements under `pkgs/`
- `patches/`: Optional patches supporting the elements under `pkgs/`
- `kernel/`: Customization of the kernel
    - `config`: **(REQUIRED)** The kconfig file for this board. Can be a symlink
	- `src.yml`: If present, used as the source of the Linux kernel instead of the
	  generic mainline kernel. Can be a symlink
	- `kmods/*.bst`: If present, built and shipped with the kernel as out-of-tree
	  kernel modules. Can reference the kernel itself as `boards/BOARD/kernel/linux.bst`.
	  The modules should be unsigned and uncompressed!
- `splash.bmp`: If present, will be rendered as a splash screen by systemd-stub early during
   boot

## Creating a new board

Create a new folder in this `boards/` directory, populate it with the above described directory
structure, and that's it! The rest of the build system will automatically pick up this board

## Generated files

Due to the inherent similarity between boards, and BuildStream's layout, there would have to be
a _lot_ of repeated boilerplate code in the `elements/` directory for each board. Frankly, that's
difficult to maintain. Thus, we have this `boards/` directory, outside of `elements/`, that defines
files in such a way that we can automatically generate the boilerplate under `boards/` as-needed.

Of all the generated boilerplate, one file is most important: `elements/boards/$BOARD/$VARIANT/publish-env.bst`.
This element pulls double-duty as both the environment that `tools/publish` executes in and the content that
gets signed. Aside from the various signing tools, the following content should be present when this element
is staged:

- `/usr.tar`: An uncompressed tarball of the `/usr` tree
- `/cmdline.txt`: The kernel command line to use. A dm-verity hash will be appended during publish
- `/linux`: The Linux kernel
- `/linux.ver`: The Linux kernel's version
- `/usr/lib/modules/$KERNEL_VERSION/`: The kernel modules tree
- `/initramfs.img`: The initramfs image, with NO kernel modules in it
- `/devicetree.dtb`: Device-tree file. Only included for boards that require it
- `/splash.bmp`: Splash screen to show during early boot. Optional

The rest of the boilerplate's operation isn't crucially important to understand, but I will
still document it here. The following files are generated under `elemnts/boards/$BOARD/*`, for
BuildStream's consumption:

- `pkgs/`: Copied verbatim
- `kernel/`: Elements that make up the kernel
	- `all.bst`: Element for staging the kernel, modules, device tree
		- The kernel itself (the bzImage or equivalent) is placed into `/linux`
		- The kernel's version number is placed into `/linux.ver`
		- Compiled device tree (if applicable) is placed into `/devicetree.dtb`
		- All modules (built-in AND external) are placed into `/usr/lib/modules`
	- `linux.bst`: The element that does the kernel build
	- `kmods/`: Copied verbatim
- `splash.bst`: Only generated if `splash.bmp` exists in the board directory. Copies the file into `/splash.bmp`
- `$variant/`: Per-variant directory
	- `pkgs.bst`: A `stack` of packages to be included in this unit (including those configured in
	  the board definition file)
	- `files.bst`: A `compose` element acting on `pkgs.bst`
	- `collect-prep.bst`: A `collect-initial-scripts` element acting on `pkgs.bst`
	- `collect-setuid.bst`: A `collect-setuid` element acting on `pkgs.bst`
	- `prep-image.bst`: A `script` element that acts on `files.bst` and `prep-scripts.bst` to
	  run the prep-image commands and generate the os-release file
	- `tarball-nosetuid.bst`: Makes a tarball out of `prep-image.bst` and puts it in `/usr.tar`
	- `tarball.bst`: Injects setuid permissions from `collect-setuid.bst` into `tarball-nosetuid.bst`
	- `all.bst`: The element that generates the final output of unit, as described above

These files are generated by the `tools/board-codegen` script
