kind: meson

sources:
- kind: git_repo
  url: github:systemd/systemd-stable
  track: refs/tags/v*
  exclude:
  - "*-rc*"

#- kind: patch
#  path: patches/systemd/installer-generator.patch
- kind: patch
  path: patches/systemd/cryptsetup-systemd-empty.patch
- kind: patch
  path: patches/systemd/systemd-sysupdated.patch

depends:
- pkgs/libcap.bst
- pkgs/kmod.bst
- pkgs/pam.bst
- pkgs/acl.bst
- pkgs/xz.bst
- pkgs/zstd.bst
- pkgs/util-linux.bst
- pkgs/elfutils.bst
- pkgs/openssl.bst
- pkgs/curl.bst
- pkgs/gnutls.bst
- pkgs/pcre2.bst
- pkgs/libseccomp.bst
- pkgs/libidn2.bst
- pkgs/tpm2/tss.bst
- pkgs/pwquality.bst
- pkgs/libxkbcommon.bst
- pkgs/qrencode.bst
- pkgs/iptables.bst

build-depends:
- pkgs/python/jinja2.bst
- pkgs/cryptsetup/stage1.bst
- pkgs/gperf.bst
- pkgs/python/pyelftools.bst
- buildsystems/meson.bst

variables:
  dns-servers: "1.1.1.1 9.9.9.10 8.8.8.8 2606:4700:4700::1111 2620:fe::10 2001:4860:4860::8888"
  time-servers: "0.carbon.pool.ntp.org 1.carbon.pool.ntp.org 2.carbon.pool.ntp.org 3.carbon.pool.ntp.org"
  meson-local: >-
    -Dmode=release
    -Dfallback-hostname="%{project-name}"
    -Dsplit-bin=false
    -Dsplit-usr=false
    -Dman=false
    -Dtests=false
    -Dselinux=false
    -Dkmod=true
    -Dopenssl=true
    -Dgcrypt=false
    -Ddefault-dns-over-tls=opportunistic
    -Ddns-servers="%{dns-servers}"
    -Dntp-servers="%{time-servers}"
    -Dnetworkd=false
    -Dnologin-path=%{bindir}/nologin
    -Ddebug-shell=%{bindir}/bash
    -Ddefault-user-shell=%{bindir}/bash
    -Dhibernate=false
    -Dkernel-install=false
    -Dpamconfdir=no
    -Dfirstboot=false
    -Dusers-gid=100
    -Drpmmacrosdir=no
    -Dcreate-log-dirs=false
    -Dsysvinit-path=""
    -Dstatus-unit-format-default=combined
    -Defi=true
    -Dbootloader=true
    -Dsbat-distro=carbon
    -Dsbat-distro-summary=carbonOS
    -Dsbat-distro-url="https://carbon.sh"
    -Ddefault-compression=zstd
    -Dfirst-boot-full-preset=true

config:
  install-commands:
    (>):
    - | # Install nsswitch.conf
      install -Dm644 factory/etc/nsswitch.conf %{install-root}%{sysconfdir}/nsswitch.conf
    - | # Btrfs-enablement in tmpfiles
      sed -i 's|d /var/log|q /var/log|' %{install-root}%{libdir}/tmpfiles.d/var.conf
      sed -i 's|d /var/cache|q /var/cache|' %{install-root}%{libdir}/tmpfiles.d/var.conf
    - | # Drop systemd's default "factory" configs. We have our own
      rm -rv %{install-root}%{datadir}/factory
    - | # Delete unused X11 configs
      rm -rv %{install-root}%{sysconfdir}/X11
    - | # Give root a shell
      sed -i '/u root/s|$| /usr/bin/bash|' %{install-root}%{libdir}/sysusers.d/basic.conf

public:
  bst:
    split-rules:
      devel:
        (>):
        - "%{libdir}/systemd/ukify"
        - "%{libdir}/systemd/boot/efi/*.stub"
