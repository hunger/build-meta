kind: cmake

sources:
- kind: git_repo
  url: github:llvm/llvm-project.git
  exclude:
  - llvmorg-*-init
  - llvmorg-*-rc*

depends:
- pkgs/libffi.bst

build-depends:
- pkgs/python/all.bst
- pkgs/libxcrypt.bst
- buildsystems/cmake.bst

# TODO: Drop libxcrypt dependency once commit lands in stable tag
# https://github.com/llvm/llvm-project/commit/d7bead833631486e337e541e692d9b4a1ca14edd
# manual-updates

variables:
  cflags-debug: "-g1"
  optimize-debug: false
  cmake: cmake -B%{build-dir} -Hllvm -G"%{generator}" %{cmake-args}
  cmake-local: >-
    -DLLVM_ENABLE_PROJECTS="clang;lld;compiler-rt"
    -DLLVM_ENABLE_ASSERTIONS:BOOL=OFF
    -DBUILD_SHARED_LIBS:BOOL=OFF
    -DLLVM_BUILD_LLVM_DYLIB:BOOL=ON
    -DLLVM_LINK_LLVM_DYLIB:BOOL=ON
    -DLLVM_ENABLE_LIBCXX:BOOL=OFF
    -DLLVM_ENABLE_ZLIB:BOOL=ON
    -DLLVM_ENABLE_FFI:BOOL=ON
    -DLLVM_ENABLE_RTTI:BOOL=ON
    -DLLVM_INCLUDE_TESTS:BOOL=OFF
    -DLLVM_INCLUDE_EXAMPLES:BOOL=OFF
    -DLLVM_INCLUDE_UTILS:BOOL=ON
    -DLLVM_INSTALL_UTILS:BOOL=ON
    -DLLVM_INCLUDE_DOCS:BOOL=OFF
    -DLLVM_ENABLE_DOXYGEN:BOOL=OFF
    -DLLVM_BUILD_EXTERNAL_COMPILER_RT:BOOL=ON
    -DLLVM_BINUTILS_INCDIR=%{includedir}
    -DLLVM_INSTALL_TOOLCHAIN_ONLY:BOOL=OFF
    -DCMAKE_C_FLAGS_RELWITHDEBINFO="-DNDEBUG"
    -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="-DNDEBUG"

# TODO: This is a workaround for https://github.com/llvm/llvm-project/issues/50459
config:
  configure-commands:
    (<):
    - sed -i '/${CLANG_COMPILER_RT_CMAKE_ARGS}/a\               -DCMAKE_LINKER=/usr/bin/ld' clang/runtime/CMakeLists.txt

public:
  bst:
    split-rules:
      devel:
        (>):
        - '%{debugdir}%{bindir}/**'
        - '%{debugdir}%{libdir}/LLVMgold.so.debug'
        - '%{bindir}/**'
        - '%{libexecdir}/**'
        - '%{libdir}/clang'
        - '%{libdir}/clang/**'
        - '%{datadir}/clang'
        - '%{datadir}/clang/**'
        - '%{datadir}/opt-viewer'
        - '%{datadir}/opt-viewer/**'
        - '%{datadir}/scan-build'
        - '%{datadir}/scan-build/**'
        - '%{datadir}/scan-view'
        - '%{datadir}/scan-view/**'
