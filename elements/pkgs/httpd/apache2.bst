kind: autotools
description: |
  Apache HTTP Server (httpd)
  Currently, it's configured to only run webdav for gnome-user-share

sources:
- kind: tar
  url: https_files:archive.apache.org/dist/httpd/httpd-2.4.57.tar.bz2
- kind: tar
  url: https_files:archive.apache.org/dist/apr/apr-1.7.4.tar.bz2
  directory: srclib/apr
- kind: tar
  url: https_files:archive.apache.org/dist/apr/apr-util-1.6.3.tar.bz2
  directory: srclib/apr-util
# release-monitoring: 1335
# Track httpd (stable) from: https://httpd.apache.org/download.cgi
# Track APR from: http://apr.apache.org/

depends:
- pkgs/pcre.bst
- pkgs/gdbm.bst
- pkgs/expat.bst
- pkgs/libxcrypt.bst

build-depends:
- buildsystems/autotools.bst

variables:
  conf-args: "%{conf-global} %{conf-local}"
  conf-local: >-
    --with-included-apr
    --enable-layout=carbon
    --enable-pie
    --enable-mpms-shared=all
    --enable-modules=none
    --enable-unixd=shared
    --enable-dav=shared
    --enable-auth-digest=shared
    --enable-authn-core=shared
    --enable-authn-file=shared
    --enable-auth-digest=shared
    --enable-authz-core=shared
    --enable-authz-user=shared
    --enable-authz-groupfile=shared

config:
  configure-commands:
    (<):
    - | # Configure the layout
      cat << EOF >> config.layout
      <Layout carbon>
          prefix:          %{prefix}
          exec_prefix:     %{prefix}
          bindir:          %{bindir}
          sbindir:         %{bindir}
          libdir:          %{libdir}
          libexecdir:      %{libdir}/httpd/modules
          installbuilddir: %{libdir}/httpd/build
          mandir:          %{mandir}
          sysconfdir:      %{sysconfdir}/httpd
          datadir:         /srv/httpd
          iconsdir:        /srv/httpd/icons
          htdocsdir:       /srv/httpd/htdocs
          manualdir:       /srv/httpd/htdocs/manual
          cgidir:          /srv/httpd/cgi-bin
          errordir:        /srv/httpd/error
          includedir:      %{includedir}/httpd
          localstatedir:   %{localstatedir}
          runtimedir:      /run/httpd
          logfiledir:      %{localstatedir}/log/httpd
          proxycachedir:   %{localstatedir}/cache/httpd
      </Layout>

      EOF
  install-commands:
    (>):
    - | # Cleanup
      rm -rf %{install-root}%{sysconfdir}          # Config files
      rm -rf %{install-root}/srv                   # Server root
      rm -rf %{install-root}/run                   # Runtime directory
      rm -rf %{install-root}%{localstatedir}       # State directory

      # Delete all the binaries except for httpd and some build-time libs
      find %{install-root}%{bindir} -type f -not -name httpd \
      -not -name apxs -not -name *-config \
      -delete

      # Delete devel files from lib
      find %{install-root} -name *.exp -delete

public:
  bst:
    split-rules:
      devel:
        (>):
        - "%{bindir}/apxs"
        - "%{libdir}/httpd/build"
        - "%{libdir}/httpd/build/**"
