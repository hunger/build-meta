kind: autotools
description: The GNU Compiler Collection

sources:
- kind: gnu
  name: gcc

depends:
- pkgs/glibc.bst
- pkgs/binutils.bst
- pkgs/m4.bst
- pkgs/zlib.bst
- pkgs/gmp.bst
- pkgs/mpfr.bst
- pkgs/mpc.bst

build-depends:
- pkgs/libxcrypt.bst
- buildsystems/bootstrap.bst

environment:
  SED: "sed" # Prevents hard-coded /tools/bin/sed
  LT_SYS_LIBRARY_PATH: "%{libdir}"

variables:
  build-dir: "build"
  conf-local: >-
    --enable-languages=c,c++,lto
    --enable-threads=posix
    --enable-default-pie
    --enable-default-ssp
    --disable-libssp
    --disable-multilib
    --disable-werror
    --disable-bootstrap
    --enable-lto
    --enable-cet=auto
    --with-system-zlib
    --enable-linker-build-id
    --with-linker-hash-style=gnu
    --enable-deterministic-archives
    --enable-shared
    --with-build-time-tools=/usr

config:
  configure-commands:
    (<):
    - | # Fixup GCC's paths: dynamic loader & libraries are in /usr/lib
      sed -e 's@/lib64/ld@%{prefix}/lib/ld@g' -i.orig gcc/config/i386/linux64.h
      sed -e '/m64=/s/lib64/lib/' -i.orig gcc/config/i386/t-linux64
  install-commands:
    (>):
    - | # Needed by FHS for historical reasons
      ln -srv %{install-root}/usr/bin/cpp %{install-root}/usr/lib/cpp
    - | # Create cc command
      ln -srv %{install-root}/usr/bin/{g,}cc
