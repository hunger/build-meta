# carbonOS - Patches

carbonOS has a very strong no-patch recommendation. Ideally, this directory wouldn't exist, but some of these patches are necessary for security (CVEs) or just to get 
the software to compile.

Contributors: Please avoid adding files to this directory. Thank you!
